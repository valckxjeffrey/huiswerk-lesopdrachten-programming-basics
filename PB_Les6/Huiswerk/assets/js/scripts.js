// HUISWERKOPDRACHT 1
// Functie die een Fibonaccireeks genereerd
// startNum is het getal waarbij de reeks moet beginnen
// maxNum is het getal tot waar de reeks moet lopen
function fibonacci(startNum, maxNum) {
    let secondNumber = 0;
    let firstAndSecond = startNum + secondNumber;
    // Deze loop genereerd de reeks
    while (firstAndSecond < maxNum) {
        // Deze regel logt bij iedere keer dat de loop rond gaat de uitkomst in de console
        console.log(firstAndSecond);
        secondNumber = startNum;
        startNum = firstAndSecond;
        firstAndSecond = startNum + secondNumber;
    }
}
// Hier wordt de functie aangeroepen met zijn parameters
fibonacci(1, 1000);

// HUISWERKOPDRACHT 2
// Functie die aftelt en vervolgens het jaartal weergeeft
function countDown(seconds) {
    // Wanneer er nog seconden over zijn, runt de funtie deze code
    if(seconds) {
        // Toont hoeveel seconden er nog te gaan zijn
        console.log(seconds+ " seconden te gaan...");
        // Iedere seconde wordt er één seconde van het totaal afgeteld
        setTimeout(countDown, 1000, --seconds);
    } else {
        // Hier wordt er een nieuw date-object aangeroepen
        var d = new Date;
        // Hier vragen we het huidige jaartal op binnen het date-object
        var currentYear = d.getFullYear();
        // Toont het huidige jaartal in de console
        console.log(`Happy ${currentYear}! 🎊🎊🎊`);
    }
}

// Hier wordt de functie aangeroepen met als parameter het aantal seconden
countDown(10);

// HUISWERKOPDRACHT 3
// Hoisting: gebruiken voordat je declareert
// Functie eerst gebruiken en dan pas declareren.
declare();

function declare() {
    console.log('Declareren Werkt');
}

// Resultaat: Werkt

//Functie-expressie gebruiken en dan pas declareren
console.log(expression);

const expression = function expression() {
    console.log('Expression Werkt');
}
// Resultaat: Werkt niet; geeft foutmelding.
