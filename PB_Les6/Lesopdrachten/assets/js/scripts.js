// Lesvoorbeeld 1
function giveMeSomeNiceName(gender) {
    let myNewName = '';
    if(gender == 'male') {
        myNewName = 'Jaap';
    } else {
        myNewName = 'Katrien';
    }

    return myNewName;
}

const myName = giveMeSomeNiceName('female');
console.log(`My new name is ${myName}`);

// A piggy function
function pig(numberOfPigs) {
    
    let showPiggies = "";
    
    for(var i = 0; i < numberOfPigs; i++) {
      showPiggies += " 🐷";
    }
    console.log(showPiggies + 'Knor');
}

pig(5); 

// Factorize a number
function facorial(num) {
    var facSum = 1;
    for (var f = 1; f <= num; f++){
      facSum = facSum*f;
    }
    console.log(`${num}! is gelijk aan ${facSum}`);
  }
  
  facorial(5);

  // In one command line
  var result  = '';
  for(var i = 1; i < 11; i += 1) {
    result = result + i;
  }
  console.log(result);

function writeASound(animal) {
    switch(animal) {
        case 'cat':
        console.log('Meow! 😼');
        break;
        case 'dog':
        console.log('WOOF! 🐶');
        break;
        case 'duck':
        console.log('KWAAAK! 🦆');
        break;
        default: 
        console.log('I do not know this animal!');
    }
}

writeASound('duck');

function hearASound(animal){

    let catSound = new Audio(src='assets/mp3/cat.mp3');
    let dogSound = new Audio(src='assets/mp3/dog.mp3');
    let duckSound = new Audio(src='assets/mp3/duck.mp3');
 
    if (animal == 'cat') {
    catSound.play();
    console.log('Meow! 😼');
    } else if (animal == 'dog') {
    dogSound.play();
    console.log('WOOF! 🐶');
    } else if (animal == 'duckie') {
    duckSound.play();
    console.log('KWAAAK! 🦆');
    }
 }
 
const handleAnimalSound = function(func, animal){
    func(animal);
}

handleAnimalSound(hearASound, 'dog');