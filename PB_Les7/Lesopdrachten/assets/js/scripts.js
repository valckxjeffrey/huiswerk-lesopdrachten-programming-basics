// Lesopdracht 1: Maken van een array met een 
// Manier 1
const productsList = [];
productsList[0] = 'tandenborstel';
productsList[1] = 'deodorant';
productsList[2] = 'bakmeel';
productsList[3] = 'wortels';
productsList[4] = 'tandpasta';
productsList[5] = 'kropsla';
productsList[6] = 'maggi';
productsList[7] = 'croky chips';
productsList[8] = 'wc papier';
productsList[9] = 'shampoo';

console.table(productsList);

// Manier 2
productsListTwo = ['tandenborstel', 'deodorant', 'bakmeel', 'wortels', 'tandpasta', 'kropsla', 'maggi', 'croky chips', 
'wc papier', 'shampoo'];

// Producten pushen naar de array door array.push
productsListTwo.push("douchegel"); 

// Laatste product verwijderen door array.pop
productsListTwo.pop();

console.table(productsListTwo);

// Array ForEach-functie
productsListTwo.forEach(function(elem) {
    console.log('element', elem);
});

// For loop om alles uit array te halen
for(index = 0; index < productsListTwo.length; index++) {
    const element = productsListTwo[index];
    console.log(`Op ${index} staat ${element}`);
}

// Zet Product en Nummer voor het product met de map-functie
let fullArrayInfo = productsListTwo.map(function(element, index) {
    let fullElement = `Product ${index}` + ` ${element}`;
    return fullElement;
});

const newArray = fullArrayInfo;

let newArrayLength = newArray.length;
console.log(newArrayLength);

for(i = 0; i < newArrayLength; i++) {
    console.log(newArray[i]);
}

