// HUISWERKOPDRACHT 1
// Allereerst een array maken met de waarden erin
const lapRounds = [2.99, 3.00, 3.01, 4.01, 2.79, 2.88, 3.10, 4.12];
// Functie definiëren
function randomLapRound() {
    // De door Math.random gepakte waarde wordt in de variabele randomLapRound gezet
    var randomLapRound = lapRounds[Math.floor(lapRounds.length * Math.random())];
    // Deze variabele wordt in de console weergegeven
    console.log(`De random waarde uit de array is ${randomLapRound}`);
}
// Functie aanroepen
randomLapRound();

// HUISWERKOPDRACHT 2
// Allereerst een array definiëren
const allMyRecords = [
    ["The Who - Pinball Wizard", "Rolling Stones - Exile on main street", "Police - Message in a bottle"],
    ["De Dijk - Alle 40 Goed", "Het Goede Doel - Belgie", "Doe Maar - skunk"]
 ];
// Loop 1: Pakt de eerste dimentie van de array
for(let a = 0; a < allMyRecords.length; a++) {
    // Zet de uitkomst in een variabele zodat er ook mee gerekend kan worden
    let recordsPartOne = allMyRecords[a];
    // Loop 2: Pakt de tweede dimentie van de array door de lengte van de variabele hierboven te nemen
    // en de overige waarden weer te geven
    for(let b = 0; b < recordsPartOne.length; b++) {
        // Console log resultaten van beide loops (A & B)
        console.log("Dimensie [" + a + "] Record [" + b + "] = " + recordsPartOne[b]);
    }
}

// HUISWERKOPDRACHT 3
// Originele code uit de opdracht om het resultaat te bekijken
const filteredLapRoundsWithForLoop = function () {
    let newArray = [];
    for (let i = 0; i < lapRounds.length; i++) {
 
        if (lapRounds[i] < 4) {
            newArray.push(lapRounds[i]);
        }
    }
    console.log(newArray);
 }

filteredLapRoundsWithForLoop();

// Resultaat van de code hierboven: Toont alles boven de 4
// Nu met filter...
// Kwam er niet uit dus heb het op internet gezocht
// Eerst een functie aanmaken om te bepalen waar de gefilterde array aan moet voldoen
function everythingUnderFour(number) {
    return number < 4;
}
// Nu de functie als filter toepassen op de lapRounds-array en deze in de variabele lapRoundsUnderFour zetten
  let LapRoundsUnderFour = lapRounds.filter(everythingUnderFour);
  // Variabele letRoundsUnderFour bevat de gefilterde waarden, deze in console.log bevat dezelfde array
  // als het voorbeeld hierboven
  console.log(LapRoundsUnderFour);
