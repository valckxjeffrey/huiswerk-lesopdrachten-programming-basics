// Lesvoorbeeld 1: While loop
/*
message = 'Het is eindelijk weekend';

var i = message.length;
console.log(message.length);

while(i>=0){
    i--;
    console.log(message.charAt(i));
} */

// Zet nummers 1-10 in de console
let i = 0;
while (i < 11) {
    console.log(i);
    i++;
}

// Zet alle letters van 'Het is maandag' in de console
let message = 'Het is maandag';
var a = message.length;

while(a>=0){
    a--;
    console.log(message.charAt(a));
} 

// Draait 'Geweldig' om.
let amazingString = 'Geweldig';
var temp = '';
var b = amazingString.length;

while (b > 0) {
    temp += amazingString.substring(b - 1, b);
    b--;
}

console.log(temp);

// Zet nummers 1-10 in de console (FOR)
var c;
for (c = 0; c < 11; c++) {
    console.log(c);
}

// Zet alle letters van 'Het is maandag' in de console (FOR)
let messageTwo = 'Het is maandag';

for (d=messageTwo.length; d>=0; d--) {
    console.log(messageTwo.charAt(d));
} 

// Kijken welke getallen in reeks van 1-25 deelbaar zijn door 3
for (e = 1; e <= 25; e++) {
    if(e % 3 === 0) {
        console.log(`${e} is deelbaar door 3`);
    }
}

