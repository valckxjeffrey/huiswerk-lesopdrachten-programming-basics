// Controleer reeks en kijk welke getallen deelbaar zijn door 4
let a = 0;
while (a <= 40) {
    if(a % 4 === 0) {
        console.log(`${a} is deelbaar door 4`);
    }
    a++;
}

// While loop die een Fibonacci getallenreeks genereerd
let firstNumber = 1;
let secondNumber = 0;
let firstAndSecond = firstNumber + secondNumber;
while (firstAndSecond < 1000) {
    console.log(firstAndSecond);
    secondNumber = firstNumber;
    firstNumber = firstAndSecond;
    firstAndSecond = firstNumber + secondNumber;                  
}

// Telt alle getallen in een array bij elkaar op
let Numbers = [2, 4, 8, 9, 12, 13];
let totalOfAll = 0;

for (b=0;b<Numbers.length;b++)
{
console.log(Numbers[b]);
totalOfAll = totalOfAll + Numbers[b];
}

console.log(`De totale som van alle waarden is ${totalOfAll}`);


