// In Javascript een object inladen
let Jeffrey = {
    name: "Jeffrey",
    age: 22,
    previousDiploma: "Media Development",
    car: {
        type: "Auto",
        numberOfWheels: 4,
        color: "Silver",
        brand: "Opel",
        model: "Corsa",
        year: 2006
    },
    family: ["Esther", "Danny", "Davey", "Eline", "Rambo", "Zara"],
    favoriteTransport: function() {
        return "Mijn favoriete vervoersmiddel is de " + this.car.type + " en deze heeft " + this.car.numberOfWheels + " wielen.";
    },
    printFamilyNames: function() {
        for(index = 0; index < this.family.length; index++) {
            const element = this.family[index];
            console.log(`Familielid ${index} heet ${element}`);
        }
    }
};

// DOT-notatie om properties toe te voegen.
Jeffrey.currentStudy = "HBO-ICT";
Jeffrey.hobby = "Niet echt iets specifieks";
Jeffrey.boolean = true;
Jeffrey.decimal = 1.2;
Jeffrey.symbol = Symbol('foo');
Jeffrey.undefined = undefined;
Jeffrey.null = null;

// Geeft alles uit het object weer
console.log(Jeffrey);

/*
// Favoriete vervoersmiddel 
console.log(`Mijn favoriete vervoersmiddel is de ${Jeffrey.car.type} en deze heeft ${Jeffrey.car.numberOfWheels} wielen.`);

// For loop om alles uit array 'family' te halen
for(index = 0; index < Jeffrey.family.length; index++) {
    const element = Jeffrey.family[index];
    console.log(`Familielid ${index} heet ${element}`);
}
*/

// Print de zin van de functie favoriteTransport uit het Jeffrey-object
let FavoriteTransportSentence = Jeffrey.favoriteTransport();
console.log(FavoriteTransportSentence);

// Print alle familienamen door de functie printFamilyNames uit het object Jeffrey te gebruiken
Jeffrey.printFamilyNames();

 