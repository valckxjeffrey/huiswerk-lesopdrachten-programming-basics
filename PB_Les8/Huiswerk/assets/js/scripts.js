// HUISWERKOPDRACHT 1
lapRoundsObject = {
    "lapRounds": [55.99,  63.00, 63.01, 54.01, 62.79, 52.88, 53.10, 54.12]
};

// HUISWERKOPDRACHT 2
// Array met leraren en de daarbij horende informatie
const teachers = [
    // Nieuw object
    {
        name: "Loek",
        profession: "Teacher",
        brand: "Linux",
        hoursPerWeek: 40, 
        salary: 2000,
        salaryPerHour: function() {
        return this.salary / (4 * this.hoursPerWeek)
        }
    },
    // Nieuw object
    {
        name: "Daan",
        profession: "Teacher",
        brand: "Arduino",
        hoursPerWeek: 40,
        salary: 3500,
        salaryPerHour: function() {
        return this.salary / (4 * this.hoursPerWeek)
        }
    },
    // Nieuw object
    {
        name: "Rimmert",
        profession: "Teacher",
        brand: "Apple",
        hoursPerWeek: 40,
        salary: 2800,
        salaryPerHour: function() {
        return this.salary / (4 * this.hoursPerWeek)
        }
    }
];

// For loop die alles uit de objecten haalt
for(index = 0; index < teachers.length; index++) {
    // Zet de benodigde informatie in variabelen
    teacherName = teachers[index].name;
    teacherProfession = teachers[index].profession;
    teacherBrand = teachers[index].brand;
    teacherSalary = teachers[index].salaryPerHour();
    // Geef de informatie weer in een zin
    console.log(`I have a ${teacherProfession} called ${teacherName} and he likes to work on a ${teacherBrand} computer. He earns €${teacherSalary} per hour.`);
}

// HUISWERKOPDRACHT 3 (Gelukt met behulp van een studentassistent :))
// ZIE TOEVOEGING AAN HUISWERKOPDRACHT 2