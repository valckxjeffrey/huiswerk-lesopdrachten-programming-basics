// Definieert currentAction en geeft de variabele een waarde
let currentAction = 'Strings';
// Toont de inhoud van currentAction in de console
console.log(currentAction);

// Wanneer currentAction 'Calculating' is, doe het volgende...
if (currentAction == 'Calculating') {
    console.log(`De waarde van de currentAction is Calculating.`);

    const a = 20;
    const b = 30;

    console.log(`Waarde A is ${a} en Waarde B is ${b}.`);

    console.log(`Wanneer je waarde A en B bij elkaar optelt krijg je ${a + b}.`);

    console.log(`Wanneer je waarde A aftrekt van waarde B krijg je ${b - a}.`);

    console.log(`Wanneer je waarde B aftrekt van waarde A krijg je ${a - b}.`);

    console.log(`Wanneer je waarde A en B met elkaar vermenigvuldigt krijg je ${a * b}.`);

    console.log(`Wanneer je waarde A deelt door waarde B krijg je ${a / b}.`);

    console.log(`Wanneer je waarde B deelt door waarde A krijg je ${b / a}.`);

    // Wanneer currentAction 'Strings' is, doe het volgende...
} else if (currentAction == 'Strings') {
    let theString = 'De waarde van de currentAction is Strings.';

    let theStringUpper = theString.toUpperCase();
    console.log(theStringUpper);

    let theStringLength = theString.length;
    console.log(`De zin is ${theStringLength} tekens lang.`)

    let theStringMatch = theString.match(/waarde/g);
    console.log(theStringMatch);

    let theStringSub = theString.substring(0, 10);
    console.log(theStringSub);

    let theStringCharAt = theString.charAt(0);
    console.log(`De zin begint met de letter ${theStringCharAt}.`);

    // Wanneer currentAction 'Booleans' is, doe het volgende...
} else if (currentAction == 'Booleans') {
    const eindCijfer = 8;

    if (eindCijfer > 6) {
        console.log(`Het eindcijfer is hoog genoeg, geslaagd!`);
    } else {
        console.log(`Het eindcijfer is te laag, volgende keer beter!`);
    }

} else {
    console.log(`Oeps! Geen overeenkomsten.`)
}

// Bereken het BMI 
let mass = 65;
let length = 1.83;
let bioMassIndex = mass / (length * length);
let bioMassIndexExp = bioMassIndex.toFixed(2);

// Toont berekend BMI in de console...
console.log(`Je BMI is ${bioMassIndexExp}`);

// Wanneer BMI onder de 18.5 is, doe het volgende...
if (bioMassIndexExp < 18.5) {
    console.log(`Ondergewicht...`);
    // Wanneer BMI tussen de 18.5 en 24.9 is, doe het volgende...
} else if (18.5 <= bioMassIndexExp && bioMassIndexExp <= 24.9) {
    console.log(`Normaal gewicht`);
    // Wanneer BMI tussen de 25 en de 29.9 is, doe het volgende...
} else if (25 <= bioMassIndexExp && bioMassIndexExp <= 29.9) {
    console.log(`Overgewicht`);
} else {
    // Wannneer BMI niet voldoet aan de gestelde eisen...
    console.log(`Obesitas`);
}

