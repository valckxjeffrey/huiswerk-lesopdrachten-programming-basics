// Definieert eindcijfer en geef de variabele een waarde
const endGrade = 9.0;

// Toont het eindcijfer in de console
console.log(`Het eindcijfer dat je hebt gehaald is ${endGrade}.`);

// Opdracht 1
// Wanneer eindcijfer onder de 6 is, doe het volgende...
if (endGrade < 6) {
    console.log(`Je hebt een onvoldoende...`);
    // Wanneer eindcijfer tussen de 6 en de 7 is, doe het volgende...
} else if (6 <= endGrade && endGrade < 7) {
    console.log(`Je hebt een voldoende!`)
    // Wanneer eindcijfer tussen de 7 en de 9 is, doe het volgende...
} else if (7 <= endGrade && endGrade < 9) {
    console.log(`Je hebt een goed gescoord!`)
    // Wanneer het niet voldoet aan de voorwaarden, doe het volgende...
} else {
    console.log(`Je hebt het uitmuntend gedaan!`)
}

// Opdracht 2
switch (true) {
    // Wannneer eindcijfer onder de 6 is, doe het volgende...
    case (endGrade < 6):
        console.log(`Je hebt een onvoldoende...`);
        break;
    // Wannneer eindcijfer onder de 7 is, doe het volgende...    
    case (endGrade < 7):
        console.log(`Je hebt een voldoende!`);
        break;
    // Wannneer eindcijfer onder de 9 is, doe het volgende...
    case (endGrade < 9):
        console.log(`Je hebt een goed gescoord!`);
        break;
    // Wanneer het niet voldoet aan de voorwaarden, doe het volgende...
    default:
        console.log(`Je hebt het uitmuntend gedaan!`);
        break;
}

// Opdracht 3
// Definieer de variabelen en geef ze een waarde...
let purchasedBook = true;
let job = `teacher`;
let inTrain = true;

// Wanneer purchasedBook false is, geef een melding...
if (purchasedBook == false) {
    console.log('First, purchase the book...')
    // Wanneer job niet gelijk is aan 'teacher', geef een melding...
} else if (job !== 'teacher') {
    console.log('First, become a teacher...');
    // Wanneer inTrain false is, geef een melding...
} else if (inTrain == false) {
    console.log('First, get on the next train...');
    // Wanneer alle variabelen kloppen, geef de volgende melding...
} else {
    console.log('Finally, I can enjoy my book!')
}