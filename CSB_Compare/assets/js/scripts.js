// Variabelen uit de opdracht
const compareNumberFive = 5,
compareNumberFiveAgain = 5.0;

// Vergelijking van die variabelen
let numberComparison = compareNumberFive == compareNumberFiveAgain;
console.log(numberComparison);

// Variabelen uit de opdracht
const compareNumberTen = 10,
compareNumberTenAgain = "10";

// Vergelijking van die variabelen
let numberComparisonTwo = compareNumberTen == compareNumberTenAgain;
console.log(numberComparisonTwo);

console.log(compareNumberTen);
console.log(compareNumberTenAgain);

// Variabelen uit de opdracht
const compareNumberTwenty = 10,
compareNumberWithString = "Hoe werkt dit?";

// Vergelijking van die variabelen
let numberComparisonThree = compareNumberTwenty == compareNumberWithString;
console.log(numberComparisonThree);
