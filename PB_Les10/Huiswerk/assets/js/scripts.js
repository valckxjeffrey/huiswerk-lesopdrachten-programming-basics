let removeLink = document.getElementById('deleteButton');
// Opdracht 1
// Allereerst een counter definiëren en deze tijdelijk op 0 zetten omdat er nog niet geklikt is.
let counter = 0;

// Vervolgens controleren of de counter 0 is, zo ja; nieuw H1-element aanmaken en in DOM zetten
if(counter == 0) {
    // Plaats waar het H1-Element moet komen
    const placeOfNewClicker = document.getElementById('clickerContainer');
    // H1-Element aanmaken
    const newClicker = document.createElement('h1');
    // H1-Element het ID 'showCounter' meegeven
    newClicker.setAttribute('id', 'showCounter');
    // Nieuwe element de inhoud van de counter als tekst meegeven
    newClicker.innerText = counter;
    // Nieuwe element pushen naar DOM in de DIV 'ClickerContainer'
    placeOfNewClicker.appendChild(newClicker);
} 

// Button ophalen uit DOM en deze in de variabele 'getClicker' zetten
const getClicker = document.getElementById('clickerButton');

// Functie die de waarde van counter controleert en het ID bij bepaalde waarde aanpast
function checkCounter(counter) {
    if(counter == 10) {
        getClicker.id = 'clickerButtonSmaller';
    } else if (counter == 20) {
        getClicker.id = 'clickerButtonSmallest';
    }
}

// Wanneer button wordt ingedrukt, voer functie clickEventHandler uit
getClicker.addEventListener('click', clickEventHandler);

// Functie die uitgevoerd moet worden wanneer de knop wordt ingedrukt
function clickEventHandler(event) {
    // Counter ophalen
    const getCounter = document.getElementById('showCounter');
    // Per druk op de knop 1 optellen bij de waarde van de counter
    counter++;
    // Nieuwe waarde toevoegen
    getCounter.innerText = counter;
    // Functie checkCounter uitvoeren om te controleren of de waarde 10 of 20 is
    checkCounter(counter);
    }



// OPDRACHT 2
// Add-button ophalen uit DOM en deze in de variabele 'getAddButton' zetten
const getAddButton = document.getElementById('submitButton');

// Wanneer button wordt ingedrukt, voer functie clickEventHandler uit
getAddButton.addEventListener('click', groceryClickEventHandler);

// Variabele voor totaalprijs definiëren en deze de waarde 0 meegeven
let totalPrice = 0;

// Wanneer er een nieuw item wordt toegevoegd, voer deze functie uit
function groceryClickEventHandler(event) {
    // PreventDefault zorgt ervoor dat de pagina niet refresht
    event.preventDefault();
    // Haal inputwaarde van productnaam op
    const inputText = document.getElementById('textInput').value;
    // Haal inputwaarde van productprijs op
    const priceText = document.getElementById('priceInput').value;
    // Wanneer inputtext leeg is, geef een alert weer
    if(inputText == '') {
        alert('Voer iets in!');
    } else {
    // Haal de tabel op
    const placeOfNewEntry = document.getElementById('groceryTable');
    // Maak een nieuwe rij aan
    const newListItem = document.createElement('tr');
    // Maak een nieuwe kolom aan
    const productName = document.createElement('td');
    // Zet de productnaam uit de input die eerder wordt gedefinieerd in de kolom
    productName.innerText = inputText;
    // Maak een nieuwe kolom aan
    const productPrice = document.createElement('td');
    // Zet de prijs uit de input die eerder wordt gedefinieerd in de kolom
    productPrice.innerText = `€${priceText}`;
    // Maak een nieuwe button aan
    const deleteProduct = document.createElement('a');
    // Zet 'Verwijderen' in de knop
    deleteProduct.setAttribute("id", "deleteButton"); 
    deleteProduct.setAttribute("href", "#");
    deleteProduct.innerText = 'Verwijderen';
    
    // Voeg alle childs toe aan het document
    placeOfNewEntry.appendChild(newListItem);
    newListItem.appendChild(productName);
    newListItem.appendChild(productPrice);
    newListItem.appendChild(deleteProduct);

    // Haal de totaalprijs-element op uit de HTML
    const tableTotalPrice = document.getElementById('totalPrice');
    // Maak van de prijs een float zodat ermee gerekend kan worden
    priceTextToFloat = parseFloat(priceText);
    // Bereken de nieuwe totaalprijs
    newTotalPrice = totalPrice + priceTextToFloat;
    // Zet de nieuwe totaalprijs in de totalPrice-variabele
    totalPrice = newTotalPrice;
    // Zet de totaalprijs in het document
    tableTotalPrice.innerText = `€${totalPrice}`;
    }
}

removeLink.addEventListener('click', deleteEventHandler);
function deleteEventHandler(event) {
    let tr = event.target.parentNode.parentNode;
    table.removeChild(tr);
}
