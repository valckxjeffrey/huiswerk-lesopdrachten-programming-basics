/*
// Controleert of de pagina en alle bijbehorende assets zijn ingeladen
window.addEventListener('load', init)

// Functie voert uit zodra pagina is geladen
function init(){
const getDiv = document.getElementById('container');
getDiv.addEventListener('click', clickEventHandler);
}

// Wanneer er op de knop wordt geklikt, wordt deze functie uitgevoerd
function clickEventHandler(event) {
    if(event.target.nodeName == 'IMG') {
        console.log(event.target.nodeName);
    }
}
*/

// Controleert of de pagina en alle bijbehorende assets zijn ingeladen
window.addEventListener('load', init)

// Functie voert uit zodra pagina is geladen
function init(){
const getSubmit = document.getElementById('submitButton');
getSubmit.addEventListener('click', clickEventHandler);
}

// Wanneer er op de knop wordt geklikt, wordt deze functie uitgevoerd
function clickEventHandler(event) {
    event.preventDefault();
    const inputText = document.getElementById('textInput').value;
    if(inputText == '') {
        alert('Voer iets in!');
    } else {
    const placeOfNewEntry = document.getElementById('formList');
    const newListItem = document.createElement('li');
    newListItem.innerText = inputText;
    placeOfNewEntry.appendChild(newListItem);
    }
}

