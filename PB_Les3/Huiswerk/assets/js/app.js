const checkWorking = 'Eerste test: Alles werkt naar behoren.';
console.log(checkWorking);

// --- OPDRACHT 1 --- //
// Het nummer waarvan gecontroleerd moet worden of het even of oneven is.
let numberToCheck = 20;

// Statement dat de check uitvoert met modulo en weergeeft of het nummer even of oneven is.
if (numberToCheck % 2 == 0) {
    console.log('Nummer is even.');
} else {
    console.log('Nummer is oneven.');
}

// --- OPDRACHT 2 --- //
// De string uit de opdracht
const programmingString = 'Programming is not so cool.';

// Haalt het woord 'not' uit de string
console.log(programmingString.replace('not ', ''));

// --- OPDRACHT 3 --- //

/* HET IS GEEN SLIMME VRAAG OM DIT TE DOEN OMDAT EEN STRING EN EEN NUMBER NOOIT EEN GELIJKENIS
ZULLEN HEBBEN. DIT MAAKT HET ONNODIG OM ZE UBERHAUPT TE VERGELIJKEN */

// Variabelen uit de opdracht
const compareNumber = 1400,
compareString = 'Ik woon in Naboo';

// Vergelijking van die variabelen
let stringComparison = compareNumber == compareString;
console.log(stringComparison);



