/*
// Hele lijst ophalen
let list = document.getElementsByTagName('li'); 
// Eerste item van lijst ophalen
let firstListItem = list[0];
// Kleur van eerste item veranderen
firstListItem.classList.add('standOut');
// Inhoud van eerste item ophalen met innertext
let FirstListItemText = firstListItem.innerText;
console.log(FirstListItemText);


const placeOfNewChild = document.getElementById('groceryList');

const newListItem = document.createElement('li');
newListItem.innerText = 'kropsla'; 

placeOfNewChild.appendChild(newListItem);*/


// Nieuw object
let groceryList = [
    {
        'productName': "Deodorant",
        'productPrice': 2.99
    },
    {
        'productName': "Bakmeel",
        'productPrice': 0.99
    },
    {
        'productName': "Cadeaukaart",
        'productPrice': 10.00
    },
    {
        'productName': "Appeltaart",
        'productPrice': 2.99
    },
    {
        'productName': "Frikandellenbroodje",
        'productPrice': 1.50
    }
];

// Functie schrijven zodat de data in de tabel in de html komt
function createMyAwesomeTable() {
    for (r = 0; r < groceryList.length; r++) {
        const table = document.createElement('table');
        const tr = document.createElement('tr');
        const tdName = document.createElement('td');
        tdName.innerText = groceryList[r].productName;
        const tdPrice = document.createElement('td');
        tdPrice.innerText = groceryList[r].productPrice;
 
        tr.appendChild(tdName);
        tr.appendChild(tdPrice);
        table.appendChild(tr);
 
        const body = document.getElementById('body');
        body.appendChild(table);
    }
}
 createMyAwesomeTable();

