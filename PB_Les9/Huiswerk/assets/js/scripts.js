// OPDRACHT 1

// Allereerst de tablerows ophalen en deze in een variabele zetten
const list = document.getElementsByTagName('tr');
// Vervolgens een array definiëren, deze blijft nog leeg en wordt gevuld in de forloop die komt
let gradesArray = [];
// Vervolgens een variabele definiëren, deze blijft ook nog leeg en wordt ook gevuld in de for loop
let gradesArraySum = 0; 

// For loop die door de tabel gaat
for (let index = 0; index < list.length; index++) {
    // Ieder Tabelitem apart in een variabele zetten
    let listItem = list[index];
    // Vervolgens van het laatste element van de tabelrij de inhoud ophalen
    let getGrade = listItem.lastElementChild.innerText;
    // Vervolgens omzetten in een float omdat het decimale getallen bevat en ermee gerekend moet worden
    let parsedValue = parseFloat(getGrade);
    // Vervolgens de eerder aangemaakte array vullen
    let generateArray = gradesArray.push(parsedValue);
    // Vervolgens de zojuist opgehaalde waarde optellen bij de variabele gradesArraySum en deze in de variabele
    // averageGrade zetten
    let averageGrade = gradesArraySum += gradesArray[index];
    };

    // Gemiddelde cijfer uitrekenen
    let overallGrade = gradesArraySum / list.length;

    // Allereerst tabel ophalen
    const table = document.getElementById('table');
    // Rij toevoegen aan tabel
    let row = table.insertRow(list.length);

    // Cellen toevoegen aan rij
    let addFirstCell = row.insertCell(0);
    let addSecondCell = row.insertCell(1);

    // Tekst en gemiddelde cijfer in zojuist aangemaakte cellen zetten
    addFirstCell.innerText = 'Gemiddelde cijfer';
    addSecondCell.innerText =  overallGrade;

    // OPDRACHT 2
    // Allereerst de lijst ophalen
    const getList = document.getElementsByTagName('li');

    // Voer deze functie altijd uit
    function giveColorToEven(getList) {
        // For loop om ieder lijstitem op te halen
        for (let listIndex = 0; listIndex < getList.length; listIndex++) {
            // Ieder lijstitem ophalen en in een variabele zetten
            const listElement = getList[listIndex];
            // Classnames van ieder lijstitem ophalen en in een variabele zetten
            classNames = getList[listIndex].className;
            // Vervolgens alleen het cijfer uit het lijstitem halen
            var getClassNameNumber = classNames.substr(6, 1); 

            // Vergelijken of het nummer dat uit de class is gehaald even is
            // Zo ja, voeg de class aquamarine toe
            if(getClassNameNumber % 2 == 0) {
            listElement.classList.add('aquamarine');
            }
        }
    }

    // Functie uitvoeren
    giveColorToEven(getList);

    // OPDRACHT 3
    // Functie om een IMG-element aan te maken
    function createImageElement(imageSrcName) {
        // Plaats waar IMG-element moet komen en in een variabele zetten
        let imagePlace = document.getElementById('imageDiv');
        // IMG-element aanmaken
        var createImage = document.createElement("IMG");
        // Pad van afbeelding in een variabele zetten
        imageSrcName = '../Huiswerk/assets/img/dog.jpg'; 
        // Source toevoegen aan IMG-element
        createImage.src = imageSrcName; 
        // Image pushen naar DOM
        imagePlace.appendChild(createImage);
    }

    // Functie uitvoeren
    createImageElement();